<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductSeeder::class);
    }
}

class ProductSeeder extends Seeder
{
    public function run(){
        DB::table('products')-> insert([
            [
                'nombre' => 'USB Kingston',
                'precio' => 3.50,
                'descripcion' => 'USB pen, 50Gb.',
                'puntuacion' => 4.5,
                'categoria' => 'otros',
                'img' => 'https://media.kingston.com/kingston/product/ktc-product-usb-dt100g3-dt100g332gb-3-sm.jpg'                    
            ],
            [
                'nombre' => 'Portatil Lenovo',
                'precio' => 1000.50,
                'descripcion' => 'Portatil Lenovo Intel i9.',
                'puntuacion' => 4.3,
                'categoria' => 'portatiles',
                'img' => 'https://images-na.ssl-images-amazon.com/images/I/61YCOI6ToFL._AC_SX466_.jpg'                 
            ],
            [
                'nombre' => 'Portatil Macintosh',
                'precio' => 2500,
                'descripcion' => 'Portatil Mac Intel i9',
                'puntuacion' => 4.5,
                'categoria' => 'portatiles',
                'img' => 'https://tiendaselectron.com/69342-large_default/port%C3%A1til-apple-de-338cm-133-macbook-pro-2020-mwp42ya-i5-16gb-512gb-ssd.jpg'                
            ],
            [
                'nombre' => 'Cascos Logitech',
                'precio' => 30.25,
                'descripcion' => 'Cascos Logitech dolby surround.',
                'puntuacion' => 3.5,
                'categoria' => 'cascos',
                'img' => 'https://images-na.ssl-images-amazon.com/images/I/51fmZmEYULL._AC_SX466_.jpg'               
            ]
        ]);

    }
}