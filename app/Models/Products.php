<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    // FILTRO MENU IZQUIERDA (CATEGORIAS)
    public function scopeCatMenu($query) {
        return $query->select('categoria')->distinct();
        //return $query->join('users','tutorials.author','=','users.id')->where('tutorials.id','=',$idTut);
    }
    // FILTRO CATEGORIA
    public function scopeCategory($query,$categoria) {
        return $query->where('categoria','=',$categoria);
        //return $query->join('users','tutorials.author','=','users.id')->where('tutorials.id','=',$idTut);
    }
    // FILTRO PUNTUACION
    public function scopePuntuacion($query,$estrellas) {
        return $query->whereIn('puntuacion',$estrellas);
    }
    // FILTRO SORT
    public function scopeSort($query,$sort) {
        return $query->orderBy('precio',$sort);
    }
}
