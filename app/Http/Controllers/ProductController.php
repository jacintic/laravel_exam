<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{    
   
    private $products;
    private $_filters;

    public function __construct()
    {
        $this->productModel= new Product();
        $menu = $this->productModel->query();
        $menu->CatMenu();
        return view('products')->with('categorias',$menu->get());
    }
    /**
     * Method to list all the products
     */
    public function all()
    {   
        $products = $this->productModel->query();
        return view('products')->with('productos',$products->get());
    }

    /**
     * Method to list the products filtered by category
     */
    public function category(Request $request)
    {   
        $categoria = $request->categoria;
        $products = $this->productModel->query();
        $products->Category($categoria);
        return view('products')->with('productos',$products->get());
    }

    /**
     * Method to list the products filtered by stars
     */
    public function stars(Request $request)
    {
        $stars = $request->puntuacion;
        $products = $this->productModel->query();
        $products->Puntuacion($stars);
        return view('products')->with('productos',$products->get());
    }
    /**
     * Method to list the products filtered by stars
     */
    public function sort(Request $request)
    {
        $sort = $request->orderby;
        $products = $this->productModel->query();
        $products->Sort($sort);
        return view('products')->with('productos',$products->get());
    }
    /**
     * Method to list the products filtered by stars
     */
    ///////////////////////////////////////////////////////////////
    // METODO QUE COMBINA TODOS LOS METODOS ANTERIORES Y ORDER BY
    ///////////////////////////////////////////////////////////////
    public function master(Request $request)
    {
        $stars = $request->puntuacion;
        $categoria = $request->categoria;
        $sort = $request->orderby;
        $products = $this->productModel->query();
        if ($stars) {
            $products->Puntuacion($stars);
        }
        if ($categoria) {
            $products->Category($categoria);
        }
        if ($sort) {
            $products->Sort($sort);
        }
        return view('products')->with('productos',$products->get());
    }
}